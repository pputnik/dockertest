FROM ubuntu:18.04
RUN apt-get update && \
    apt-get upgrade -y  && \
    apt-get autoremove && apt-get clean && \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    rm -rf /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir /var/www

WORKDIR /var/www
COPY index.html .
COPY entrypoint.sh .

RUN sed -i "s/\$built/$(date)/" index.html 

#RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

# for utf-8 on ubuntu
#RUN apt-get install -y locales && rm -rf /var/lib/apt/lists/* && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
#ENV LANG en_US.utf8

#EXPOSE 80
CMD ./entrypoint.sh

