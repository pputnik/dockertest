variable "name" {
  description = "Role name"
  default     = ""
}

variable "service" {
  description = "Service which will use the role, e.g.: ec2.amazonaws.com, lambda.amazonaws.com"
  default     = ""
}

variable "policy_arns" {
  description = "Optional. List of ARNs of the policy to attach to the role"
  default     = []
}

variable "tags" {
  description = "tags to apply to all underlying resources"
  default     = {}
}

variable "description" {
  description = "Role description"
  default     = ""
}
