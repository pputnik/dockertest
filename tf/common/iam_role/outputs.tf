output "arn" {
  description = "Role ARN"
  value = aws_iam_role.this.arn 
}

output "name" {
  description = "Role ARN"
  value = aws_iam_role.this.name
}

output "unique_id" {
  description = "The stable and unique string identifying the role"
  value = aws_iam_role.this.unique_id 
}