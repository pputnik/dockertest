provider "aws" {
  region = var.region
}

terraform {
  required_version = ">= 0.12"
}

terraform {
  backend "s3" {
    bucket               = "alex-test-iz"
#    workspace_key_prefix = "acle"
    key                  = "terraform.tfstate"
    encrypt              = "true"
    region               = "eu-west-1"
  }
}
