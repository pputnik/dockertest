resource "aws_ecs_cluster" "tailer_cluster" {
  name = "${local.project}_tailer_cluster_${terraform.workspace}"
  capacity_providers = [ "FARGATE" ]
  tags  = local.tags
}

data "template_file" "tailer_task" {
  template = file("tailer_task.json")

  vars = {
    name        = "${local.project}_tailer_${terraform.workspace}"
    tailer_img  = var.tailer_image
    subname     = "${local.project}_${terraform.workspace}"
    region      = var.region
    workspace   = terraform.workspace
    project     = local.project
    account     = data.aws_caller_identity.identity.account_id
  }
}

resource "aws_ecs_task_definition" "tailer_task" {
  family = "Tailer"
  execution_role_arn = module.tailer_task_role.arn
  requires_compatibilities = [ "FARGATE" ]
  network_mode = "awsvpc"
  cpu = 256
  memory = 512
  container_definitions = data.template_file.tailer_task.rendered
}

resource "aws_ecs_service" "tailer" {
  name          = "${local.project}_tailer_service_${terraform.workspace}"
  cluster       = aws_ecs_cluster.tailer_cluster.id
  desired_count = 1

  # Track the latest ACTIVE revision
  task_definition = aws_ecs_task_definition.tailer_task.family
  launch_type = "FARGATE"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent = 300
  lifecycle {
    ignore_changes = [ desired_count ]
  }
  network_configuration {
    subnets = [data.aws_ssm_parameter.subnets.value]
    assign_public_ip = true
  }
}

# autoscaling
# https://medium.com/@bradford_hamilton/deploying-containers-on-amazons-ecs-using-fargate-and-terraform-part-2-2e6f6a3a957f