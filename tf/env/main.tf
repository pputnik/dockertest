### role to run container
module "tailer_task_role" {
  source          = "../common/iam_role"

  name          = "${local.project}_tailer_task_${terraform.workspace}"
  service       = "ecs-tasks.amazonaws.com"
  policy_arns   = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
  ]
  description   = true
  tags  = local.tags
}

data "template_file" "tailer_ssm_policy" {
  template = file("tailer_ssm_policy.json")
  vars = {
    project     = local.project
    account     = data.aws_caller_identity.identity.account_id
  }
}

resource "aws_iam_policy" "tailer_ssm_policy" {
  name = "tailer_ssm_policy"
  policy = data.template_file.tailer_ssm_policy.rendered
}

resource "aws_iam_role_policy_attachment" "tailer_task_role" {
  role       = module.tailer_task_role.name
  policy_arn = aws_iam_policy.tailer_ssm_policy.arn
}
### /role to run container