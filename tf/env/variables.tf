locals {
  project = "tda"
  tags = {
    "Responsible" = "Alex"
    "Environment" = terraform.workspace
  }
}

variable "region" {
    default = "eu-west-1"
}

variable "tailer_image" {
}

data "aws_ssm_parameter" "subnets" {
  name = "tda_subnet_${terraform.workspace}"
  #arn = "arn:aws:ssm:*:172748671118:parameter/tda_subnet_${terraform.workspace}"
}

data "aws_caller_identity" "identity" {}