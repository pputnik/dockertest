#!/bin/bash

# project name: will be in stack name and other places
proj="tda"
# timeout to create stack, in minutes
stack_timeout=10   

if [ "x$2" = "x" ] ; then
    echo Usahe: $0 stageName dockerImg
    exit -1
fi
stage=$1
docker_img=$2
name="$proj-$stage"

date_now=$(date +"%F-%H%M")
stack_timeout_sec=$(( stack_timeout * 60 ))

tags="--tags Key=Name,Value=$name  Key=Responsible,Value=Alex"
stackname="--stack-name $name"
capab="--capabilities CAPABILITY_IAM"
templatebody="--template-body file://$proj.yml"
stack_timeout="--timeout-in-minutes $stack_timeout"
#params="--parameters file://cicd.params"
params="--parameters ParameterKey=DockerImg,ParameterValue=$docker_img ParameterKey=Environment,ParameterValue=$stage ParameterKey=Subnets,ParameterValue=tda_subnet_$stage"

#===============================
set -e
echo -n "checking if exists "
status=$(aws cloudformation describe-stacks $stackname 2>&1| grep 'StackStatus"' | awk -F'"' '{print $4}')
if [ "$status" == "ROLLBACK_FAILED" ] || [ "$status" == "ROLLBACK_COMPLETE" ] || [ "$status" == "CREATE_FAILED" ] || [ "$status" == "DELETE_FAILED" ] || [ "$status" == "ROLLBACK_IN_PROGRESS" ] || [ "$status" == "DELETE_IN_PROGRESS" ] || [ "$status" == "UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS" ] ; then
        echo " $status, try to delete"
        aws cloudformation delete-stack $stackname
        x=20
        while [ $x -gt 0 ]; do
            status=$(aws cloudformation describe-stacks $stackname 2>&1 | grep 'StackStatus"' | awk -F'"' '{print $4}')
            echo "$status"
            if [ "x$status" == "x" ]; then
                x=0
                echo creating....
                aws cloudformation create-stack $stackname $stack_timeout $templatebody $tags $capab $params
            fi
            x=$(($x-1))
            sleep 2
        done
elif [ "$status" == "CREATE_COMPLETE" ] || [ "$status" == "UPDATE_ROLLBACK_COMPLETE" ] || [ "$status" == "UPDATE_COMPLETE" ] || [ "$status" == "UPDATE_ROLLBACK_FAILED" ] ; then
        echo "exist, ($status). Starting update: "
        aws cloudformation update-stack $stackname $templatebody $tags $capab $params
else
        echo " not exist: $status"

        echo creating ....
        aws cloudformation create-stack $stackname $stack_timeout $templatebody $tags $capab $params

fi

# check progress
while [ $stack_timeout_sec -gt 0 ]; do
    status=$(aws cloudformation describe-stacks $stackname 2>&1 | grep 'StackStatus"' | awk -F'"' '{print $4}')
    echo "$stack_timeout_sec $status"
    if [ "$status" == "ROLLBACK_IN_PROGRESS" ] || [ "$status" == "UPDATE_ROLLBACK_IN_PROGRESS" ] ; then
        # we won't wait until it's complete
        echo Failed
        exit -1
    elif [ "x$status" == "x" ] || [ "$status" == "CREATE_COMPLETE" ] || [ "$status" == "UPDATE_COMPLETE" ] ; then
        echo OK
        stack_timeout_sec=0
        exit 0
    fi
    stack_timeout_sec=$(($stack_timeout_sec-5))
    sleep 5
done
# we did not receive good response
exit -1

echo end
