#!/bin/bash

if [ "x$1" = "x" ] ; then
    echo commit comment required
    exit 1
fi

python -c 'import yaml, sys; yaml.safe_load(sys.stdin)' < .gitlab-ci.yml
if [ $? -ne 0 ]; then
    echo ".gitlab-ci.yml syntax error"
    exit 1
fi

aws sts get-caller-identity >/dev/null
if [ $? -eq 255 ]; then
    echo authenticate to aws first
    exit 1
fi
export AWS_PROFILE=iz   

runner_id=i-074b54c4f958a2076
echo -n "runner state: "
aws ec2 start-instances --instance-ids $runner_id --query 'StartingInstances[].CurrentState.Name' --output text

#d=$(date +"%F %H:%M:%S")
#sed -i "s/^date: .*$/date: $d/" README.md

#rm -rf files/lambda-update-user-pool.py.zip

git pull
git add .
git status
git commit -m "$1 $2 $3 $4 $5 $6 $7 $8 $9"
git push # origin alex

aws ec2 describe-instances --no-paginate --filters Name=tag:Name,Values=runner --query 'Reservations[].Instances[].PublicIpAddress' --output text
