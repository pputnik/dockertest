set runner_id=i-074b54c4f958a2076
set AWS_PROFILE=iz
aws ec2 start-instances --instance-ids %runner_id% --query "StartingInstances[].CurrentState.Name" --output text
aws ec2 describe-instances --no-paginate --filters Name=tag:Name,Values=runner --query "Reservations[].Instances[].PublicIpAddress" --output text